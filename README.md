# Breaking Lab presents: Die Dino-KI

Auf unserem [YouTube-Kanal "Breaking Lab"](https://www.youtube.com/channel/UCE2hJ9CYR57BYhk3TjGVG6w) haben wir eine Reihe über KI gestartet. In diesem Repository bauen wir mit euch zusammen eine KI, die das [Dino-Spiel in Chrome](https://www.giga.de/downloads/google-chrome/tipps/so-findet-man-das-dinosaurier-spiel-in-google-chrome/) automatisch spielt.

## Ziel der Übung
Unser Code soll die folgenden Funktionen haben:
* Bildschirm auslesen (aktuelle Spielsituation, Punktestand)
* Arrow Key Event triggern
* Ein [Reinformement Learning](https://de.wikipedia.org/wiki/Best%C3%A4rkendes_Lernen) Modell trainieren, bei einem bestimmten Spiel-Zustand die Taste zu triggern
* Dieses Modell laufen lassen

## Mitmachen
* Erstelle einen GitLab-Account
* Forke dieses Repository
* Installiere Keras-OCR (https://github.com/faustomorales/keras-ocr)
* Installiere Tesseract-OCR (https://tesseract-ocr.github.io/tessdoc/Home.html#binaries)
* Korrigiere den Pfad zu Tesseract in der run.py
* Passe die Position der Frames an, indem du in der run.py "plotting" auf "True" setzt
* Teste herum und füge deine Verbesserung hinzu
* Sende einen Merge Request um deine Verbesserung in dieses Repository zu mergen